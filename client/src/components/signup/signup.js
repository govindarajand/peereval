import React from 'react';

import './signup.css';

class SignUp extends React.Component{
    constructor(props){
        super(props);
        this.state = {};
    }
    handleInputChange = (event) =>{
        this.setState({[event.target.name] : [event.target.value]});
    }
    handleSubmit = () => {
        fetch('/makeNewUser', {
            method: 'POST',
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state)
        });
    }
    render(){
        return (<div className="signup-container">
                    <form>
                        <span>Name</span><input name="Name" type="text" onChange={this.handleInputChange}/><br/>
                        <span>User Name</span><input name="UserName" type="text" onChange={this.handleInputChange}/><br/>
                        <span>Password</span><input name="Password" type="password" onChange={this.handleInputChange}/><br/>
                    <button onClick={this.handleSubmit}>Submit</button>
                    </form>
                </div>);
    }
}

export default SignUp;