import React from 'react';
import {connect} from 'react-redux';
import {updateUserFavs} from '../../actions/userAction';
import './home.css';

const mapStateToProps = state => {
    return {Ptiles_fav: state.tiles_fav, Ptiles_trend: state.tiles_trending}
}

const mapDispatchToProps = dispatch => {
    return {
        updateUserFavorites: user_favs => dispatch(updateUserFavs(user_favs))
    }
}
class Home extends React.Component{
    //OnStart send the username and get the details such as favorites and its corresponding images
    componentWillMount = () => {
        fetch('/userHome', {
            method: 'POST',
            headers: {
                'Accept' : 'application/json',
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({"username":this.props.username[0]})
        }).then(res => {
            if(res.ok){
               return res.json();
            }
            else{
                throw new Error(res.statusText);
            }
        }).then(favorites => {
           this.props.updateUserFavorites(favorites.resUserFav);
        }).catch(errorText => {
            console.log(errorText);
        })
    }
    render(){
        //Fill the details for favorite tiles
        if(this.props.Ptiles_fav.length !== 0){
            this.listFav = Object.values(this.props.Ptiles_fav[0]);
            return (<div>
                <br />
                    <div className="top-part">
                        <div>
                            Start a new evaluation topic?<br />
                            <button>Create</button>
                        </div>
                    </div>
                    <br />
                    <div className="bottom-part">
                        <div>
                        <span className="home-title">Your list</span><br />
                            {this.listFav.map(element => (
                                <div className="home-tile">{element}</div>)
                            )}
                        </div>
                    </div>
                    </div>);
        }
        else{
            return null;
        }

    }
}

const HomeComponent = connect(mapStateToProps,mapDispatchToProps)(Home);

export {HomeComponent as Home};