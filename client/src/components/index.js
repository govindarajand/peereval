import {Login} from './login/login';
import NavBar from './navbar/navbar';
import SignUp from './signup/signup';
import {Initial} from './initial/initial';
import {Home} from './home/home';
export {Login, NavBar, SignUp,Initial,Home};