import React from "react";
import {Login,SignUp,NavBar,Home} from "../index.js";
import {connect} from "react-redux";
import {userDetails} from "../../actions/userAction.js";
import "./initial.css";

const mapStateToProps = state => {
  return {PUserName: state.user_details};
};
const mapDispatchToProps = dispatch => {
  return {
    update_userDetails: userName => dispatch(userDetails(userName))
  };
}
class Initial extends React.Component{
    constructor(props){
        super(props);
        //Need to get from server the login status
        this.state = {loggedIn: false, signUp:true, failedLoginTry:false};
        this.checkLogin();
      }
      //Need to handle when clicked SignUp
      checkLogin = () => {
        if(!this.state.loggedIn){
          //Bug - Only when login is pressed twice, the home page opens
          this.pageToRender = (<Login 
                  loginStatus={this.state.loggedIn} 
                  onLogin={this.changeLoginStatus} 
                  onSignUp={this.renderSignUp}
                  failedLogin={this.state.failedLoginTry}
                  />);
          this.userName = "Sign in";
        }
        else{
          //Home page for user
          this.pageToRender = (<div>Yet to come</div>);
          this.setState(this.state);
        }
      }
      changeLoginStatus = (loginStatus) => {
        this.setState({loggedIn:true});
      }
      renderSignUp = (userSignUp) => {
        this.pageToRender = (<SignUp />);
        this.setState(this.state);
      }
      componentDidMount = () =>{
            
      }
      componentDidUpdate = (prevProps) => {
        if(this.props.PUserName !== prevProps.PUserName && this.state.loggedIn){
          fetch('/userDetails',{
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
             },
             body: JSON.stringify(this.props.PUserName)
          })
          .then(res => {
            if(res.ok){
              res.json();
            }
            else{
              throw Error(res.statusText);
            }
          })
          .then(SuserName => {
            //Update the redux so that it stores the loggedIn user name
            // this.props.update_userDetails(SuserName.userName);
              this.pageToRender = (<Home username={this.props.PUserName}/>);
              this.fetchUserName();
              this.setState({loggedIn:true});
          }).catch((errorText) => {
            this.setState({failedLoginTry: true})
              this.pageToRender = (<Login 
                loginStatus={this.state.loggedIn} 
                onLogin={this.changeLoginStatus} 
                onSignUp={this.renderSignUp}
                failedLogin={this.state.failedLoginTry}
                />);
              this.setState({loggedIn: false});
          });
        }
      }
      fetchUserName = () => {
        this.userName = this.props.PUserName[0];
      }
      render() {
        return (<div>
                  <NavBar username={this.userName}/>
                  {this.pageToRender}
                  </div>
        );
      }
}

const InitialComponent = connect(mapStateToProps,mapDispatchToProps)(Initial);
export {InitialComponent as Initial};
