import {USER_DETAILS, TILES_FAV, TILES_TREND, UPDATE_USER_FAVS} from "../constants/action-types";

const initialState = {
    user_details: [],
    tiles_fav: [],
    tiles_trending: []
}
export default function userReducer(state = initialState, action){
    switch(action.type){
        case USER_DETAILS:
            return {...state,user_details:[...state.user_details, action.payload]}
        case TILES_FAV:
            return {...state}
        case TILES_TREND:
            return {...state,tiles_trending:[...state.tiles_trending, action.payload]}
        case UPDATE_USER_FAVS:
            return {...state,tiles_fav:[...state.tiles_fav, action.payload]}
        default:
            return state;
    }
}