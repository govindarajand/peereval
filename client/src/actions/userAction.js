import {USER_DETAILS, TILES_FAV,TILES_TREND, UPDATE_USER_FAVS} from "../constants/action-types";

export const userDetails = (PuserName) => ({type: USER_DETAILS, payload: PuserName});

export const tilesFav = (PtilesFav) => ({type:TILES_FAV, payload: PtilesFav});

export const tilesTrend = (PtilesTrend) => ({type:TILES_TREND, payload: PtilesTrend});

export const updateUserFavs = (favorites) => ({type: UPDATE_USER_FAVS, payload: favorites});