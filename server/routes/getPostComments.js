const express = require('express');
const getPostCommentsRouter = express.Router();
const mongoUtil = require('../mongoUtil');
const logger = require('../logger');

getPostCommentsRouter.post('/',(req,res,next) => {
    mongoClient = mongoUtil.getClient();
    if(mongoClient !== null){
        var peerEvalDB = mongoClient.db('peerEvalSite');
        var evaluationCommentsCollection = peerEvalDB.collection('evaluationComments');
        var commentForPost= req.body['postId'];
        //Sort array by Upvotes and Creation date
        evaluationCommentsCollection.find({"postId":commentForPost}).sort({"upvotes":1, "createdOn":1}).toArray((err,result) => {
            if(result){
                res.send(result);
            }
            else{
                res.statusMessage = "Comments yet to be added";
                res.status(404).end();
            }
        });
        
    }
    else{
        logger.error("Error connecting to the MongoDB");
    }
});

module.exports = getPostCommentsRouter;
