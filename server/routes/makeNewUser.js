const express = require('express');
const makeNewUserRouter = express.Router();
const mongoUtil = require("../mongoUtil");
var mongoClient = null;

makeNewUserRouter.post('/', (req, res) => {
    mongoClient = mongoUtil.getClient();
    if(mongoClient !== null){
        var peerEvalUserDB = mongoClient.db("peerEvalUser");
        var userDetailsCollection = peerEvalUserDB.collection("userDetails");

        userDetailsCollection.insertOne({
            "userName": req.body.UserName[0],
            "FullName": req.body.Name[0],
            "Password": req.body.Password[0]
        });
    }
    else{
        logger.error("Error connecting to the MongoDB");
    } 
});


module.exports = makeNewUserRouter;