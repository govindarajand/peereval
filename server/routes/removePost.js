const express = require('express');
const removePostRouter = express.Router();
const mongoUtil = require('../mongoUtil');
const logger = require('../logger');

var mongoClient = null;

removePostRouter.post('/', (res, req, next) => {
    mongoClient = mongoUtil.getClient();
    if(mongoClient != null){
        var peerEvalDB = mongoClient.db("peerEvalSite");
        var evaluationPostsCollection = peerEvalDB.collection("evaluationPosts");
        //Check if the post was created by the user
        evaluationPostsCollection.deleteOne({
            "postId":req.body["postID"]
        },(error, result) => {
            if(error){
                res.statusMessage = "Unable to delete. Please try again";
                res.status(503).end();
            }
            else{

            }
        });
    }
});

module.exports = removePostRouter;