const express = require('express');
const userDetailsRouter = express.Router();
const mongoUtil = require('../mongoUtil');
var logger = require('../logger');

var mongoClient = null;
var userNameRetrieved = null;
var userToFind = null;

//Get user details from the mongoDB
userDetailsRouter.post('/', (req,res,next) => {
    mongoClient = mongoUtil.getClient();
    if(mongoClient !== null){
        var peerEvalUserDB  = mongoClient.db("peerEvalUser");
        var userDetailsCollection = peerEvalUserDB.collection("userDetails");
        userToFind = req.body["username"];
        userDetailsCollection.findOne({"userName": userToFind}).then((doc) => {
            if(!doc){
                res.statusMessage = "Username not found. Have you registered?";
                res.status(404).end();
            }
            else{
                userNameRetrieved = doc.userName;
                //After getting the userName from mongoDB send the userName
                if(userNameRetrieved){
                    res.send({"userName": userNameRetrieved});
                }
                else{
                    res.statusMessage = "Error retrieving Username";
                    res.status(404).end();
                }
            }
        });
    }
    else{
        logger.error("Error connecting to DB");
    }
});

module.exports = userDetailsRouter;