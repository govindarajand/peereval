const express = require('express');
const votePostRouter = express.Router();
const mongoUtil = require('../mongoUtil');
const logger = require('../logger');

var mongoClient = null;
var counterInc = 0;
var voterType = null;

votePostRouter.post('/', (req,res,next) => {
    mongoClient = mongoUtil.getClient();
    if(mongoClient !== null){
        var peerEvalDB = mongoClient.db('peerEvalSite');
        var postVotesCollection = peerEvalDB.collection('evaluationPostVotes');
        var requestBody = req.body;
        var postToFind = requestBody['postId'];
        //First check if the user wants to remove his vote. If so, we remove it from downvote or upvote
        //after checking in which list the user exists.
        //$pull removes an element from the array
        if(requestBody["voteType"] == ""){
            postVotesCollection.updateOne({"postId": postToFind},{
                $pull: {
                    "downvoters": requestBody["username"]
                }
            }, (error, result) => {
                if(result.modifiedCount > 0){
                    postVotesCollection.updateOne({"postId": postToFind},{
                        $inc: {
                            "voteCount": 1
                        }
                    });
                }
                else{
                    postVotesCollection.updateOne({"postId": postToFind},{
                        $pull: {
                            "upvoters": requestBody["username"]
                        }
                    }, (error, result) => {
                        if(result.modifiedCount > 0){
                            postVotesCollection.updateOne({"postId": postToFind},{
                                $inc: {
                                    "voteCount": -1
                                }
                            });
                        }
                    });
                }
            });
        }
        //If the user wants to upvote, we check if he already exists in dowvoters. If so we remove the user from that list
        // and then add him to the upvoters. We increase the count by 2, since removal of downvote -(-1) and another upvote (+1)
        if(requestBody["voteType"] == "up"){
            voterType = "upvoters";
            postVotesCollection.updateOne({"postId": postToFind},{
                $pull: {
                    "downvoters": requestBody["username"]
                },
            }, (error,result) => {
                if(result.modifiedCount > 0){
                    counterInc = 2;
                }
                else{
                    counterInc = 1;
                }
            });
        }
        //For the user who downvotes, the logic is just opposite of the above code.
        else if(requestBody["voteType"] == "down"){
            voterType = "downvoters";
            postVotesCollection.updateOne({"postId": postToFind},{
                $pull: {
                    "upvoters": requestBody["username"]
                },
            }, (error,result) => {
                if(result.modifiedCount > 0){
                    counterInc = -2;
                }
                else{
                    counterInc = -1;
                }
            });
        }
        //After making all changes, in the below code is where we update the DB.
        //We add the user to the corresponding list and then we change the counter accordingly.
        if(requestBody["voteType"] !== ""){
            postVotesCollection.updateOne({"postId": postToFind},{
                    $addToSet: {
                        [voterType]: requestBody["username"]
                    }
                }, (error,result) => {
                    if(result.modifiedCount > 0){
                        postVotesCollection.updateOne({"postId": postToFind},{
                            $inc: {
                                "voteCount": counterInc
                            }
                        });
                    }
            });
        }
        res.end();
    }
    else{
        logger.error("Error connecting to the MongoDB");
    }
    
});

module.exports = votePostRouter;
