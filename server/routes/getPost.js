const express = require('express');
const getPostRouter = express.Router({mergeParams:true});
const mongoUtil = require('../mongoUtil');
const logger = require('../logger');

getPostRouter.get('', (req,res,next) => {
    mongoClient = mongoUtil.getClient();
    var peerEvalSiteDB  = mongoClient.db("peerEvalSite");
    var evaluationPostsCollection = peerEvalSiteDB.collection('evaluationPosts');
    evaluationPostsCollection.findOne({"postId":req.params.postId}).then((doc) => {
        if(!doc){
            res.statusMessage = "Problem with fetching the post";
            res.status(404).end();
        }
        else{
            res.send({"post":doc});
        }
    });
});

module.exports = getPostRouter;