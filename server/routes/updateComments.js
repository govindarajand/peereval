const express = require('express');
const updateCommentsRouter = express.Router();
const mongoUtil = require('../mongoUtil');
const uuid = require('uuid/v4');
const logger = require('../logger');

updateCommentsRouter.post('/',(req,res,next) => {
    mongoClient = mongoUtil.getClient();
    if(mongoClient !== null){
        var peerEvalDB = mongoClient.db('peerEvalSite');
        var evaluationPostsCollection = peerEvalDB.collection('evaluationPosts');
        //Change it here to request body
        var requestBody = req.body;
        var postToFind = requestBody['postId'];
        evaluationPostsCollection.updateOne({"postId":postToFind},{
            $push: {
                "comments": {
                    "createdOn": Date.now() + "",
                    "createdBy": requestBody['username'][0],
                    "content": requestBody['content'],
                    "commentId": uuid(),
                    "hiearchy": 0,
                    "upvotes": 0
                }
            }
        });
        res.end();
    }
    else{
        logger.error("Error connecting to the MongoDB");
    }
});

module.exports = updateCommentsRouter;
