const express = require('express');
const getUserPostsRouter = express.Router();
const mongoUtil = require('../mongoUtil');
const logger = require('../logger');

getUserPostsRouter.post('/',(req,res,next) => {
    mongoClient = mongoUtil.getClient();
    if(mongoClient !== null){
        var peerEvalDB = mongoClient.db('peerEvalSite');
        var evaluationPostsCollection = peerEvalDB.collection('evaluationPosts');
        var userToFind = req.body['username'][0];
        evaluationPostsCollection.find({"createdBy":userToFind}).toArray((err,result) => {
            if(result.length > 0 && result){
                res.send(result);
            }
            else{
                res.statusMessage = "Trouble finding your Posts. Did you add them?";
                res.status(404).end();
            }
        });
    }
    else{
        logger.error("Error connecting to the MongoDB");
    }
});

module.exports = getUserPostsRouter;
