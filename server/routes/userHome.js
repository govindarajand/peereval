const express = require('express');
const userHomeRouter = express.Router();
const mongoUtil = require('../mongoUtil');
const logger = require('../logger');

var mongoClient = null;
var userFavorites = null;

userHomeRouter.post('/', (req,res,next) => {
    mongoClient = mongoUtil.getClient();
    if(mongoClient !== null){
        var peerEvalDB = mongoClient.db("peerEvalUser");
        var peerEvalSiteDB = mongoClient.db("peerEvalSite");
        var userDetailsCollection = peerEvalDB.collection("userDetails");
        var websiteResourcesCollection = peerEvalSiteDB.collection('websiteResources');
        var userToFind = req.body['username'];
        userDetailsCollection.findOne({"userName": userToFind}).then((doc) => {
            if(!doc){
                res.statusMessage = "Trouble finding you";
                res.status(404).end();
            }
            else{
                userFavorites = doc.favorites;
                var userFavArray = Object.values(userFavorites);
                if(userFavorites){
                    websiteResourcesCollection.find({"topic":{$in: userFavArray}}).project({_id:0, "topic":1, "resources.mainLogo":1}).toArray((error,docFav) => {
                        if(!error){
                            var favImages = {};
                            docFav.forEach(element => {
                                favImages[element.topic] = element.resources.mainLogo;
                            });
                            res.send({
                                "resUserFav":userFavorites, 
                                "favImages": favImages
                            });
                        }
                    });
                }
                else{
                    res.statusMessage = "Trouble finding your favorites. Did you add them?";
                    res.status(404).end();
                }
            }
        });
    }
    else{
        logger.error("Error connecting to the MongoDB");
    }
});

module.exports = userHomeRouter;