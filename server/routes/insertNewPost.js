const express = require('express');
const insertNewPostRouter = express.Router();
const mongoUtil = require('../mongoUtil');
const logger = require('../logger');
const uuid = require('uuid/v4');

insertNewPostRouter.post('/',(req,res,next) => {
    mongoClient = mongoUtil.getClient();
    if(mongoClient !== null){
        var peerEvalDB = mongoClient.db('peerEvalSite');
        var evaluationPostsCollection = peerEvalDB.collection('evaluationPosts');
        var postUpvotesCollection = peerEvalDB.collection('evaluationPostUpvotes');
        var requestBody = req.body;
        var postID = uuid();
        //New post with details filled partly here
        evaluationPostsCollection.insertOne({
            "createdOn": Date.now() + "",
            "createdBy": requestBody['username'][0],
            "content": {
                "question": requestBody["question"],
                "answer" : requestBody["answer"]
            },
            "postId": postID,
            "topic": requestBody["topic"]
        }, (error, result) => {
            if(error){
                res.statusMessage = "Service unavailable. Failed to put your Post through";
                res.status(503).end();
            }
            else{
                //Post voting which is a separate Collection, is filled with empty details
                postUpvotesCollection.insertOne({
                    "postId": postID,
                    "upvoters": [],
                    "downvoters":[],
                    "voteCount": 0
                },(error,result) => {
                    if(error){
                        logger.error("Upvote Collection not accepting the record:" + postID);
                    }
                });
                res.end();
            }
        });
    }
    else{
        logger.error("Error connecting to the MongoDB");
    }
});

module.exports = insertNewPostRouter;
