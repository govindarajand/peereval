const express = require('express');
const bodyParser = require('body-parser');
const mongoUtil = require('./mongoUtil');
const session = require('express-session');
const fileStore = require('session-file-store')(session);
const uuid = require('uuid/v4');
const logger = require('./logger');

const app = express();
const PORT = process.env.PORT || 5000;

//Import Routes
const userDetailsRouter = require('./routes/userDetails');
const makeNewUserRouter = require('./routes/makeNewUser');
const userHomeRouter = require('./routes/userHome');
const getUserPostsRouter = require('./routes/getUserPosts');
const getPostCommentsRouter = require('./routes/getPostComments');
const insertNewPostRouter = require('./routes/insertNewPost');
const updateCommentsRouter = require('./routes/updateComments');
const votePostRouter = require('./routes/votePost');
const createNewTopic = require('./routes/createNewTopic');
const getPostRouter = require('./routes/getPost');

//Use functions - Stack
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

//Map Routes
mongoUtil.connectToMongoServer( (error) => {
    app.use(session({
        genid: (req) => {
            return uuid();
        },
        secret: 'modify to unique string in Prod',
        store: new fileStore(),
        resave: false,
        saveUninitialized: true
    }));

    app.get('/', (req, res) => {
        res.send();
    });
    app.use((req,res,next) => {
        if(!req.session.views){
            req.session.views = {};
        }

        req.session.views['count'] = (req.session.views['count'] || 0) + 1;
        next();
    });

    app.use('/userDetails', userDetailsRouter);
    app.use('/makeNewUser', makeNewUserRouter);
    app.use('/userHome', userHomeRouter);
    app.use('/getUserPosts', getUserPostsRouter);
    app.use('/getPostComments', getPostCommentsRouter);
    app.use('/insertNewPost', insertNewPostRouter);
    app.use('/updateComments', updateCommentsRouter);
    app.use('/votePost', votePostRouter);
    app.use('/createNewTopic', createNewTopic);
    app.use('/post/:postId',getPostRouter);
    
    app.use('/count', (req,res,next) => {
        res.send(req.session.views);
    })
});

app.listen(PORT, () =>{
    logger.info(`Server started at ${PORT}`);
});