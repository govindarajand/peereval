var MongoClient = require('mongodb').MongoClient;

var _client;
var mongoURL = "mongodb://localhost:27017/";

//Connect the MongoDB once and get the client from anywhere
module.exports = {
    connectToMongoServer: (callback) => {
        MongoClient.connect(mongoURL,{ useNewUrlParser: true },function(err,client){
            _client = client;
            return callback(err);
        });
    },
    getClient: () => {
        return _client;
    }
};